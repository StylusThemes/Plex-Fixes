# Plex Fixes [![Version][version]][1] [![Size][size]][1] [![Install directly from GitHub][install]][2] [![GitHub stars][stars]][3] [![GitHub watchers][watchers]][4] [![GitHub open issues][open issues]][5] [![GitHub closed issues][closed issues]][5] [![GitHub license][license]][6] [![GitHub last commit][last commit]][7] [![devDependencies][devdependencies]][8]

**_Fixing various issues I find with [Plex]_**

[![Preview]][screenshots]

## Menu

- [Installation]
- [Contributions]
- [Screenshots]

## Installation

1. Download one of these add-ons for your browser:
   - Stylus: [Chrome][stychrome], [Firefox][styfirefox] or [Opera][styopera].
   - xStyle: [Chrome][xstychrome], [Firefox][xstyfirefox].
2. To install directly from GitHub, click here:

   [![Install directly from GitHub][Plex Fixes]][2]

3. Done! From now on it will automatically update.

## Contributions

If you would like to contribute to this repository, please...

1. 👓 Read the [contribution guidelines][contributing].
1. ![repo-forked][9] [Fork][10] or ![cloud-download][11] [download][12] this repository.
1. 👌 Create a pull request!

## Screenshots
<details><summary>Show screenshots</summary>

[![Series]][Screenshots]

[![Movies]][Screenshots]

[![Music]][Screenshots]
</details>

<!-- BADGES -->
[version]: https://flat.badgen.net/github/release/StylusThemes/Plex-Fixes
[1]: #
[size]: https://flat.badgen.net/badgesize/normal/StylusThemes/Plex-Fixes/master/style.user.css
[install]: https://flat.badgen.net/badge/install%20directly%20from/GitHub/00ADAD "Click here!"
[2]: https://rebrand.ly/plex-fixes
[stars]: https://flat.badgen.net/github/stars/StylusThemes/Plex-Fixes
[3]: https://github.com/StylusThemes/Plex-Fixes/stargazers
[watchers]: https://flat.badgen.net/github/watchers/StylusThemes/Plex-Fixes
[4]: https://github.com/StylusThemes/Plex-Fixes/watchers
[open issues]: https://flat.badgen.net/github/open-issues/StylusThemes/Plex-Fixes
[closed issues]: https://flat.badgen.net/github/closed-issues/StylusThemes/Plex-Fixes
[5]: https://github.com/StylusThemes/Plex-Fixes/issues
[license]: https://flat.badgen.net/github/license/StylusThemes/Plex-Fixes
[6]: https://creativecommons.org/licenses/by-sa/4.0/
[last commit]: https://flat.badgen.net/github/last-commit/StylusThemes/Plex-Fixes
[7]: https://github.com/StylusThemes/Plex-Fixes/commits/master
[devdependencies]: https://flat.badgen.net/david/dev/StylusThemes/Plex-Fixes
[8]: https://david-dm.org/StylusThemes/Plex-Fixes?type=dev
[badges]: https://flat.badgen.net/badge/amount%20of%20badges/12/orange

<!-- Plex LINK -->
[Plex]: https://app.plex.tv/desktop

<!-- PREVIEW -->
[Preview]: ./images/screenshots/tv.jpg?raw=true "Click to see more screenshots"

<!-- MENU -->
[Installation]: README.md#installation
[Contributions]: README.md#Contributions
[Screenshots]: README.md#screenshots

<!-- CONTRIBUTIONS -->
[contributing]: ./.github/CONTRIBUTING.md
[9]: https://user-images.githubusercontent.com/136959/42383736-c4cb0db8-80fd-11e8-91ca-12bae108bccc.png
[10]: https://github.com/StylusThemes/Plex-Fixes/fork
[11]: https://user-images.githubusercontent.com/136959/42401932-9ee9cae0-813d-11e8-8691-16e29a85d3b9.png
[12]: https://github.com/StylusThemes/Plex-Fixes/releases

<!-- STYLUS DOWNLOADS -->
[STYChrome]: https://chrome.google.com/webstore/detail/stylus/clngdbkpkpeebahjckkjfobafhncgmne
[STYFirefox]: https://addons.mozilla.org/firefox/addon/styl-us/
[STYOpera]: https://addons.opera.com/extensions/details/stylus/

<!-- XSTYLE DOWNLOADS -->
[XSTYChrome]: https://chrome.google.com/webstore/detail/xstyle/hncgkmhphmncjohllpoleelnibpmccpj
[XSTYFirefox]: https://addons.mozilla.org/firefox/addon/xstyle/

<!-- INSTALL Plex Fixes BADGE -->
[Plex Fixes]: https://flat.badgen.net/badge/Plex%20Fixes/install/00ADAD "Click here!"

<!-- SCREENSHOTS -->
[Series]: ./images/screenshots/tv.jpg?raw=true "Series"
[Movies]: ./images/screenshots/movie.jpg?raw=true "Movies"
[Music]: ./images/screenshots/music.jpg?raw=true "Music"
